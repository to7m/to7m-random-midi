import random
from math import log2, ceil
from time import sleep
from rtmidi import MidiOut


random.seed(0)
RANDOM_NUMBER = random.getrandbits((1 << 12) + 64)


def messy_32bit_hash(int):
    hash = 0
    for bit in range(32):
        rand64 = RANDOM_NUMBER >> (hash & 0xfff)
        rand32pair = [rand64 & 0xffffffff, (rand64 >> 32) & 0xffffffff]
        match = 1 & (int >> bit)
        hash ^= rand32pair[match]
    return hash


def send_messages(seeds, message, send_message_callable, interval):
    message_low = []
    message_range = []
    for byte in message:
        if type(byte) == tuple:
            message_low.append(byte[0])
            message_range.append(byte[1] - byte[0] + 1)
        else:
            message_low.append(byte)
            message_range.append(1)
    product_of_ranges = 1
    for range_byte in message_range:
        product_of_ranges *= range_byte
    hashes_needed = ceil(ceil(log2(product_of_ranges)) / 32)
    low_seed, high_seed = seeds
    for seed in range(low_seed, high_seed + 1):
        combined_hash = 0
        for hash in range(hashes_needed):
            combined_hash <<= 32
            combined_hash += messy_32bit_hash(seed * hashes_needed + hash)
        current_message = message_low.copy()
        for index, range_byte in enumerate(message_range):
            combined_hash, diff_result = divmod(combined_hash, range_byte)
            current_message[index] += diff_result
        print(f"seed {seed}: {current_message}")
        send_message_callable(current_message)
        sleep(interval)


sender_object = MidiOut()
sender_object.open_port(3)  # or whatever number the port is

send_messages(seeds=(50, 80000),
              message=[(144, 159), (0, 127), 0],
              send_message_callable=sender_object.send_message,
              interval=0.1)
