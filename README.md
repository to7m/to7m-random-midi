# to7m-random-midi

The main breakthrough in cracking the KeyLab was in sending random messages. I wrote this Python 3 script to make it easier to send customised random messages, so anyone with an IDE can try the same thing. It requires the Python module rtmidi.

Replace the 3 in sender_object.open_port(3) with your MIDI device's port number.


In this example in the script, I've opened port 3, and run send_messages() with four arguments:

* seeds=(50, 80000)
This means send the message for seed 50, then the message for seed 51, and so on until seed 80000.

* message=[(144, 159), (0, 127), 40]
This is a list of bytes, where (144, 159) means that byte 0 should be a value from 144 to 159 (note on, any of the 16 channels), (0, 128) means that byte 1 should be a value from 0 to 127 (any velocity), and 40 means that byte 2 should always be 40.

* send_message_callable=sender_object.send_message
This can be ignored in most cases. The function/method/callable (in this case, a method from an rtmidi MidiOut object) needs to accept a single list of bytes as the message to be sent.

* interval=0.1
This is how long the send_messages() will wait between MIDI messages. Set it lower if you want to send a huge number of messages, or higher if you want to be more cautious.


WARNING: It's possible that using this script will bork your device. If you're sending SysEx messages at random like I was, I can't guarantee you won't end up bricking some expensive hardware, or wiping all your settings or something. But, like, it hasn't happened to me yet.